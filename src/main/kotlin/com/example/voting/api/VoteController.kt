package com.example.voting.api

import com.example.voting.common.BaseUrl
import com.example.voting.dao.entity.VoteEntity
import com.example.voting.dto.StatisticDTO
import com.example.voting.dto.StatisticResponseDTO
import com.example.voting.dto.VoteRequestDTO
import com.example.voting.service.VoteService
import org.springframework.cache.Cache
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping(BaseUrl.votingV1)
class VoteController(
    private val voteService: VoteService
) {

    //JPA
    @PostMapping("/jpa/save")
    fun addJpaVote(@RequestBody vote: VoteRequestDTO): VoteEntity {
        return voteService.saveJpaVote(vote)
    }
    @GetMapping("/jpa/statistic")
    fun getJpaStatistic(): StatisticDTO {
        return voteService.getJpaStatistic()
    }

    //JdbcTemplate
    @PostMapping("/jdbc/save")
    fun addJdbcVote(@RequestBody vote: VoteRequestDTO): Int {
        return voteService.saveJdbcVote(vote)
    }

    @GetMapping("/jdbc/statistic")
    fun getJdbcStatistic(): StatisticResponseDTO {
        return voteService.getJdbcStatistic()
    }

    @GetMapping("/cache")
    fun getCache(): Cache? {
        return voteService.getCache("votes")
    }
}