package com.example.voting.jdbcTemplate.dao

import com.example.voting.dto.StatisticResponseDTO
import com.example.voting.dto.VoteRequestDTO

interface VoteDao {
    fun addVote(vote: VoteRequestDTO): Int

    fun getStatistic(): StatisticResponseDTO
}