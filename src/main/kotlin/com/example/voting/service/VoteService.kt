package com.example.voting.service

import com.example.voting.dao.entity.VoteEntity
import com.example.voting.dao.repository.VoteJpaRepository
import com.example.voting.dto.StatisticDTO
import com.example.voting.dto.StatisticResponseDTO
import com.example.voting.dto.VoteRequestDTO
import com.example.voting.enum.DecisionType
import com.example.voting.jdbcTemplate.dao.VoteDao
import org.springframework.cache.Cache
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.caffeine.CaffeineCache
import org.springframework.cache.caffeine.CaffeineCacheManager
import org.springframework.stereotype.Service

@Service
class VoteService(
    private val voteJpaRepository: VoteJpaRepository,
    private val voteDao: VoteDao,
    private val cacheManager: CacheManager
) {
    //JPA
    fun saveJpaVote(vote: VoteRequestDTO): VoteEntity {
        return voteJpaRepository.save(
            VoteEntity(
                decision = vote.decision,
                uuid = vote.uuid
            )
        )
    }

    fun getJpaStatistic(): StatisticDTO {
        val yesStat = voteJpaRepository.getStatistic(DecisionType.YES)
        val noStat = voteJpaRepository.getStatistic(DecisionType.NO)
        return StatisticDTO(
            total = mapOf(
                "YES" to yesStat,
                "NO" to noStat
            )
        )
    }

    //JdbcTemplate
    fun saveJdbcVote(vote: VoteRequestDTO): Int {
        return voteDao.addVote(vote)
    }

    @CacheEvict("votes")
    fun getJdbcStatistic(): StatisticResponseDTO {
        return voteDao.getStatistic()
    }

    fun getCache(name: String): Cache? {
        return cacheManager.getCache(name)
    }
}