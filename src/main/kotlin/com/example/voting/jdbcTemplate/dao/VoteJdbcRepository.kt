package com.example.voting.jdbcTemplate.dao

import com.example.voting.dto.StatisticResponseDTO
import com.example.voting.dto.VoteRequestDTO
import com.example.voting.enum.DecisionType
import com.example.voting.jdbcTemplate.model.StatisticRowMapper
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class VoteJdbcRepository(
    private val jdbcTemplate: JdbcTemplate
) : VoteDao {
    override fun addVote(vote: VoteRequestDTO): Int {
        val sql = "INSERT INTO votes (uuid,decision) VALUES (?,?::decision_type) ON CONFLICT(uuid) DO NOTHING;"
        return jdbcTemplate.update(sql, vote.uuid, vote.decision.name)
    }

    override fun getStatistic(): StatisticResponseDTO {
        val sql = "SELECT v.decision, COUNT(*) FROM votes v GROUP BY v.decision;"
        val rsl = StatisticResponseDTO()
        jdbcTemplate.query(sql, StatisticRowMapper()).forEach { pair ->
            when (DecisionType.valueOf(pair.first)) {
                DecisionType.YES -> rsl.totalYes = pair.second
                DecisionType.NO -> rsl.totalNo = pair.second
            }
        }
        return rsl
    }
}