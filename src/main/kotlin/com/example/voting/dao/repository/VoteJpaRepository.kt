package com.example.voting.dao.repository

import com.example.voting.dao.entity.VoteEntity
import com.example.voting.enum.DecisionType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface VoteJpaRepository : JpaRepository<VoteEntity, Int> {
    @Query(
        """
    SELECT COUNT(*) FROM votes v WHERE v.decision = :decision
    """, nativeQuery = true
    )
    fun getStatistic(decision: DecisionType): Int
}