package com.example.voting.jdbcTemplate.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

data class VoteModel(
    @JsonProperty("id")
    var id: Int,
    @JsonProperty("uuid")
    var uuid: String,
    @JsonProperty("email")
    var decision: String,
    @JsonProperty("created")
    var created: LocalDateTime,
    @JsonProperty("updated")
    var updated: LocalDateTime,
)