CREATE TYPE decision_type AS ENUM ('YES', 'NO');
CREATE TABLE votes
(
    id       serial      NOT NULL primary key,
    uuid     uuid UNIQUE NOT NULL,
    decision decision_type NOT NULL
);