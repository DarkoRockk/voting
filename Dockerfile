FROM gradle:7-jdk18 as build
COPY . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle bootJar

FROM openjdk:18
COPY --from=build /home/gradle/src/build/libs/voting-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "./voting-0.0.1-SNAPSHOT.jar"]
