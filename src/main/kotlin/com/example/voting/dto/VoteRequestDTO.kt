package com.example.voting.dto

import com.example.voting.enum.DecisionType
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class VoteRequestDTO(

    @JsonProperty("uuid")
    var uuid: UUID,

    @JsonProperty("decision")
    var decision: DecisionType
)