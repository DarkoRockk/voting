package com.example.voting.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class StatisticDTO(
    @JsonProperty("total")
    var total: Map<String, Int>
)
