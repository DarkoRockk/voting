package com.example.voting.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class StatisticResponseDTO(
    @JsonProperty("totalYes")
    var totalYes: Int = 0,
    @JsonProperty("totalNo")
    var totalNo: Int = 0
)
