package com.example.voting

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@SpringBootApplication
@EnableJpaAuditing
class VotingApplication

fun main(args: Array<String>) {
	runApplication<VotingApplication>(*args)
}
