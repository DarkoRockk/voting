package com.example.voting.dao.entity

import com.example.voting.enum.DecisionType
import io.hypersistence.utils.hibernate.type.basic.PostgreSQLEnumType
import jakarta.persistence.*
import jakarta.persistence.Table

import org.hibernate.annotations.*
import java.util.*


@Entity
@Table(name = "votes")
//@TypeDef()
@DynamicUpdate
data class VoteEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int? = null,

    var uuid: UUID,

    @Column(columnDefinition = "decision_type", name = "decision")
    @Type(PostgreSQLEnumType::class)
    @Enumerated(EnumType.STRING)
    var decision: DecisionType,
)
