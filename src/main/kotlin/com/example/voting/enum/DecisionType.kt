package com.example.voting.enum

enum class DecisionType {
    YES, NO
}