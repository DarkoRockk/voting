package com.example.voting.jdbcTemplate.model

import com.example.voting.dto.StatisticResponseDTO
import com.example.voting.enum.DecisionType
import org.springframework.jdbc.core.RowMapper
import java.sql.ResultSet

class StatisticRowMapper : RowMapper<Pair<String, Int>> {
    override fun mapRow(rs: ResultSet, rowNum: Int): Pair<String, Int> {
        return Pair(
            first = rs.getString("decision"),
            second = rs.getInt("count")
        )
    }
}
